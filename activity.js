// Creating the pokemon trainer

let trainer = {
  name: "Zero",
  age: 20,
  address: {
    city: "Little Root",
    country: "Ho-En Region"
  },
  friends: ["May","Steven","Wally","Emerald","Wallace"],
  pokemon: ["Swampert","Gardevoir","Salamence","Metagross","Frosslass"],
  catch: function(newPokemon){

    if(this.pokemon.length === 6){
      alert("A Trainer should only have 6 pokemons to carry!");
    } else {
      this.pokemon.unshift(newPokemon);
      alert(`Gotcha, ${newPokemon}!`)
    }
    return newPokemon;
  },
  release: function(byePokemon){
    if(this.pokemon.length === 0){
      alert("You have no more Pokemons! Catch one First!")
    } else {
      this.pokemon.pop(byePokemon)
    }
    return this.pokemon;
  }
}
console.log(trainer);


function Pokemon(name,type,level){
  this.name = name;
  this.type = type;
  this.level = level;
  this.hp = 3*level;
  this.atk = 2.5*level;
  this.def = 2*level;
  this.isFainted = false;
  this.tackle = function(isTackled){
    console.log(`${this.name} tackled ${isTackled.name}`)
    if(isTackled.hp > 0 && isTackled.isFainted == false){
      isTackled.hp -= this.atk
    } else {
      isTackled.faint

    }
  }
  this.faint = function(this){
      this.isFainted = true;
      alert(`${this.name} has fainted`)
    }

  }


let pokemon1 = new Pokemon("Milotic","Water",50)
let pokemon2 = new Pokemon("Flygon","Dragon",45)

console.log(pokemon1);
console.log(pokemon2);
